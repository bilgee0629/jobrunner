package app

import "testing"

func TestPlanFromYaml(t *testing.T) {
	plan, err := PlanFromYaml("../tests/test.yaml")
	if err != nil {
		t.FailNow()
	}
	if plan == nil {
		t.Fail()
	}
}
