package app

import (
	"fmt"
	"testing"
)

func TestGetCpuAmount(t *testing.T) {

	num := GetCpuAmount()
	if num != 4 {
		t.Fail()
	}

}

func TestScanCpuTime(t *testing.T) {
	Monolithic = NewJobRunner()

	cpuTime, err := ScanCpuTime()
	if err != nil {
		fmt.Println(err)
		t.Fail()
	}

	fmt.Println("Cputime:", cpuTime)
}
