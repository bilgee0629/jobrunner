package app

func ConditionalInteger(condition int, oldValue int, newValue int) int {
	if oldValue == condition {
		return newValue
	}
	return oldValue
}
