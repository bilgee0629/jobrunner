package app

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

type Process struct {
	Pid             int
	CurrentMemUsage float64 //in kb
	CurrentCpuUsage float64
	utime           int
	stime           int
	cutime          int
	cstime          int
}

func (process *Process) Refresh() {
	process.UpdateCpuUsage()
	process.UpdateMemUsage()
}

func (process *Process) UpdateMemUsage() {
	memUsage, err := GetMemUsage(process.Pid)
	if err != nil {
		log.Println(err)
		//maybe remove process from jobrunner?
	}
	process.CurrentMemUsage = (float64(memUsage) * 100) / float64(Monolithic.RamAmount)
}

func (process *Process) UpdateCpuUsage() {

	file, err := ioutil.ReadFile(fmt.Sprintf("/proc/%d/stat", process.Pid))
	if err != nil {
		log.Println("can't get cpu usage for process:", process.Pid)
	}

	values := strings.Split(string(file), " ")

	oldTime := process.utime + process.stime

	utime, _ := strconv.Atoi(values[13])
	stime, _ := strconv.Atoi(values[14])

	process.utime = utime
	process.stime = stime

	cpuPercent := float64(utime+stime-oldTime) / float64(Monolithic.CpuTime) * 100.0
	process.CurrentCpuUsage = cpuPercent

}

func GetMemUsage(pid int) (int, *AppError) {

	file, err := ioutil.ReadFile(fmt.Sprintf("/proc/%d/statm", pid))
	if err != nil {
		return 0, NewAppError(err, fmt.Sprintf("can't get mem usage for process:%d", pid))
	}

	memValues := strings.Split(string(file), " ")
	rss, err := strconv.Atoi(memValues[1])
	if err != nil {
		return 0, NewAppError(err, fmt.Sprintf("can't get mem usage for process:%d", pid))
	}

	//statm shows memory amount in pages
	return rss * Monolithic.PageSize, nil
}

func StartProcess(command string) (int, error) {
	cmd := exec.Command(command)
	err := cmd.Start()
	if err != nil {
		return 0, err
	}

	return cmd.Process.Pid, nil
}

func NewProcess(command string) (*Process, error) {
	cmd := exec.Command(command)
	err := cmd.Start()
	if err != nil {
		return nil, err
	}

	return &Process{Pid: cmd.Process.Pid}, nil
}

func KillProcessAfter(seconds int, pid int) error {
	process, err := os.FindProcess(pid)
	if err != nil {
		return err
	}
	time.Sleep(time.Second * time.Duration(seconds))
	process.Kill()

	return nil
}
