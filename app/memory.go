package app

import (
	"bufio"
	"os"
	"regexp"
	"strconv"
)

func GetRamAmount() int {

	file, err := os.Open("/proc/meminfo")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	reader := bufio.NewReader(file)
	line, _, err := reader.ReadLine()
	if err != nil {
		panic(err)
	}

	re := regexp.MustCompile(`(\d+)`)
	match := re.FindStringSubmatch(string(line))
	if len(match) != 2 {
		panic("Memory information can't be found")
	}

	memAmount, err := strconv.Atoi(match[1])
	if err != nil {
		panic("Invalid memory information")
	}

	return memAmount
}
