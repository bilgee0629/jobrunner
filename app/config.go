package app

import (
	"io/ioutil"
	"strings"

	"gopkg.in/yaml.v2"
)

func ReadConfigData(filePath string) []byte {

	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		panic(err)
	}

	return data
}

func PlanFromYaml(filePath string) (*Plan, *AppError) {

	plan := Plan{}

	filePath = strings.TrimSpace(filePath)

	if err := yaml.Unmarshal(ReadConfigData(filePath), &plan); err != nil {
		return nil, NewAppError(err, "can't load yaml file")
	}

	return &plan, nil
}
