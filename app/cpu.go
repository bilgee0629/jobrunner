package app

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type CpuData struct {
	UserPeriod      int
	NicePeriod      int
	SystemPeriod    int
	SystemAllPeriod int
	IdleAllPeriod   int
	IdlePeriod      int
	IoWaitPeriod    int
	IrqPeriod       int
	SoftIrqPeriod   int
	StealPeriod     int
	GuestPeriod     int
	TotalPeriod     int
	UserTime        int
	NiceTime        int
	SystemTime      int
	SystemAllTime   int
	IdleAllTime     int
	IdleTime        int
	IoWaitTime      int
	IrqTime         int
	SoftIrqTime     int
	StealTime       int
	GuestTime       int
	TotalTime       int
}

func GetCpuAmount() int {

	cpuNum := -1

	file, err := os.Open("/proc/stat")
	if err != nil {
		panic("Can't find CPU information: " + err.Error())
	}
	defer file.Close()

	reader := bufio.NewReader(file)
	for {
		line, _, err := reader.ReadLine()
		if err != nil {
			panic("Can't find CPU information: " + err.Error())
		}

		if string(line[:3]) != "cpu" {
			break
		}

		cpuNum++
	}

	return cpuNum
}

func ScanCpuTime() (int, *AppError) {

	file, err := os.Open(fmt.Sprintf("/proc/stat"))
	if err != nil {
		return 0, NewAppError(err, " - error while scanning cpu time")
	}
	defer file.Close()

	reader := bufio.NewReader(file)

	cpus := Monolithic.CpuAmount
	for i := 0; i <= cpus; i++ {

		line, _, err := reader.ReadLine()
		if err != nil {
			return 0, NewAppError(err, " - error while scanning cpu time")
		}

		stats := strings.Split(string(line), " ")

		if i == 0 {
			//first row has excess space
			stats = append(stats[:1], stats[2:]...)
		}

		userTime, _ := strconv.Atoi(stats[1])
		niceTime, _ := strconv.Atoi(stats[2])
		systemTime, _ := strconv.Atoi(stats[3])
		idleTime, _ := strconv.Atoi(stats[4])
		ioWait, _ := strconv.Atoi(stats[5])
		irq, _ := strconv.Atoi(stats[6])
		softIrq, _ := strconv.Atoi(stats[7])
		steal, _ := strconv.Atoi(stats[8])
		guest, _ := strconv.Atoi(stats[9])
		guestNice, _ := strconv.Atoi(stats[10])

		userTime = userTime - guest
		niceTime = niceTime - guestNice

		idleAllTime := idleTime + ioWait
		systemAllTime := systemTime + irq + softIrq
		virtualAllTime := guest + guestNice
		totalTime := userTime + niceTime + systemAllTime + idleAllTime + steal + virtualAllTime

		cpuData := Monolithic.CpuData[i]

		cpuData.UserPeriod = WrapSubstract(userTime, cpuData.UserTime)
		cpuData.NicePeriod = WrapSubstract(niceTime, cpuData.NiceTime)
		cpuData.SystemPeriod = WrapSubstract(systemTime, cpuData.SystemTime)
		cpuData.SystemAllPeriod = WrapSubstract(systemAllTime, cpuData.SystemAllTime)
		cpuData.IdleAllPeriod = WrapSubstract(idleAllTime, cpuData.IdleAllTime)
		cpuData.IdlePeriod = WrapSubstract(idleTime, cpuData.IdleTime)
		cpuData.IoWaitPeriod = WrapSubstract(ioWait, cpuData.IoWaitTime)
		cpuData.IrqPeriod = WrapSubstract(irq, cpuData.IrqTime)
		cpuData.SoftIrqPeriod = WrapSubstract(softIrq, cpuData.SoftIrqTime)
		cpuData.StealPeriod = WrapSubstract(steal, cpuData.StealTime)
		cpuData.GuestPeriod = WrapSubstract(virtualAllTime, cpuData.GuestTime)
		cpuData.TotalPeriod = WrapSubstract(totalTime, cpuData.TotalTime)

		cpuData.UserTime = userTime
		cpuData.NiceTime = niceTime
		cpuData.SystemTime = systemTime
		cpuData.SystemAllTime = systemAllTime
		cpuData.IdleAllTime = idleAllTime
		cpuData.IdleTime = idleTime
		cpuData.IoWaitTime = ioWait
		cpuData.IrqTime = irq
		cpuData.SoftIrqTime = softIrq
		cpuData.GuestTime = virtualAllTime
		cpuData.TotalTime = totalTime
	}

	period := Monolithic.CpuData[0].TotalPeriod / cpus
	return period, nil
}

func WrapSubstract(a int, b int) int {
	if a > b {
		return a - b
	}
	return 0
}
