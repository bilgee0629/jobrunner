package app

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"os/exec"
)

type Group struct {
	Name               string `yaml:"Name,omitempty" json:"Name,omitempty"`
	Command            string `yaml:"Command,omitempty"`
	MaxCpuUsage        int    `yaml:"Maximum CPU Usage,omitempty" json:"Maximum CPU Usage,omitempty"`
	MaxMemUsage        int    `yaml:"Maximum Memory Usage,omitempty" json:"Maximum Memory Usage"`
	MinWorkingCopies   int    `yaml:"Minimum Working Copies,omitempty" json:"Minimum Working Copies"`
	Priority           int    `yaml:"Priority,omitempty" json:"Priority,omitempty"`
	MaxExecutionTime   int    `yaml:"Maximum Execution Time,omitempty" json:"Maximum Execution Time,omitempty"`
	SentinelCmd        string `yaml:"Sentinel,omitempty"`
	SentinelProcess    *Process
	SentinelOutput     []byte
	Process            []*Process
	CurrentMemoryUsage float64
	CurrentCpuUsage    float64
	AverageMemoryUsage float64
	AverageCpuUsage    float64
}

func (group *Group) AddProcess() error {

	process, err := NewProcess(group.Command)
	if err != nil {
		return err
	}

	group.Process = append(group.Process, process)

	return nil
}

func (group *Group) RemoveProcess() error {

	fmt.Println("remove process worked")

	if len(group.Process) == 0 {
		return errors.New("no process to remove")
	}

	//we will kill latest ones
	pid := group.Process[len(group.Process)-1].Pid
	group.Process = group.Process[:len(group.Process)-1]

	process, err := os.FindProcess(pid)
	if err != nil {
		return err
	}

	if err = process.Kill(); err != nil {
		return err
	}

	return nil
}

func (group *Group) Refresh() {

	totalMemory := 0.0
	totalCpu := 0.0

	for _, process := range group.Process {
		totalMemory += process.CurrentMemUsage
		totalCpu += process.CurrentCpuUsage
	}

	group.CurrentCpuUsage = totalCpu
	group.CurrentMemoryUsage = totalMemory

	if len(group.Process) > 0 {
		group.AverageCpuUsage = group.CurrentCpuUsage / float64(len(group.Process))
		group.AverageMemoryUsage = group.CurrentMemoryUsage / float64(len(group.Process))
	} else {
		group.AverageCpuUsage = 0.0
		group.AverageMemoryUsage = 0.0
	}

	if group.SentinelProcess == nil {
		group.StartSentinel()
	}

}

func (group *Group) RefreshProcesses() {

	for _, process := range group.Process {
		process.Refresh()
	}

}

func (group *Group) SetSentinelValue() error {

	jsonText := group.SentinelOutput

	var sentinelObject Group
	if err := json.Unmarshal([]byte(jsonText), &sentinelObject); err != nil {
		return err
	}

	group.MaxCpuUsage = sentinelObject.MaxCpuUsage
	group.MaxMemUsage = sentinelObject.MaxMemUsage
	group.MaxExecutionTime = sentinelObject.MaxExecutionTime
	group.Priority = sentinelObject.Priority
	group.MinWorkingCopies = sentinelObject.MinWorkingCopies

	return nil
}

func (group *Group) StartSentinel() error {

	if group.SentinelProcess != nil {
		return nil
	}

	cmd := exec.Command(group.SentinelCmd)

	stdoutPipe, err := cmd.StdoutPipe()
	if err != nil {
		return err
	}

	err = cmd.Start()
	if err != nil {
		return err
	}

	group.SentinelProcess = &Process{Pid: cmd.Process.Pid}

	if cmd != nil {
		go KillProcessAfter(60, group.SentinelProcess.Pid) //max execution time for sentinels is 60seconds
	}

	data := make([]byte, 10000)
	bytesCount, err := stdoutPipe.Read(data)
	if err != nil {
		return err
	}

	err = cmd.Wait()
	if err != nil {
		return err
	}

	group.SentinelOutput = data[:bytesCount]
	group.SetSentinelValue()

	return nil
}
