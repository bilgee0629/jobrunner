package app

import "strconv"

type Plan []map[string]map[string]string

//GetGroups returns all group configs in a group array
func (plan *Plan) GetGroups() (groups *[]Group) {

	groups = &[]Group{}

	for _, group := range *plan {
		for groupName, groupValues := range group {
			currentGroup := Group{}

			if len(groupName) > 0 {
				currentGroup.Name = groupName
			}

			if len(groupValues["Command"]) > 0 {
				currentGroup.Command = groupValues["Command"]
			} else {
				//group must have command
				panic("one of the groups don't contain command")
			}

			if len(groupValues["Maximum CPU Usage"]) > 0 {
				//excluding last % sign
				maxCpuUsage, err := strconv.Atoi(string(groupValues["Maximum CPU Usage"][:len(groupValues["Maximum CPU Usage"])-1]))
				if err != nil {
					panic("Maximum CPU Usage should be integer")
				}
				currentGroup.MaxCpuUsage = maxCpuUsage
			} else {
				//if Maximum CPU Usage is not defined, we will assign average value
				currentGroup.MaxCpuUsage = 95 / len(*plan)
			}

			if len(groupValues["Maximum Memory Usage"]) > 0 {
				maxMemUsage, err := strconv.Atoi(string(groupValues["Maximum Memory Usage"][:len(groupValues["Maximum Memory Usage"])-1]))
				if err != nil {
					panic("Maximum CPU Usage should be integer")
				}
				currentGroup.MaxMemUsage = maxMemUsage
			} else {
				//if Maximum Memory Usage is not defined, we will assign averge value as well
				currentGroup.MaxMemUsage = 95 / len(*plan)
			}

			if len(groupValues["Minimum Working Copies"]) > 0 {
				minWorkingCopies, err := strconv.Atoi(groupValues["Minimum Working Copies"])
				if err != nil {
					panic("Error in YAML file " + err.Error())
				}
				currentGroup.MinWorkingCopies = minWorkingCopies
			} else {
				//default value is 1
				currentGroup.MinWorkingCopies = 1
			}

			if len(groupValues["Priority"]) > 0 {
				priority, err := strconv.Atoi(groupValues["Priority"])
				if err != nil {
					panic("Error in YAML file " + err.Error())
				}
				currentGroup.Priority = priority
			}

			if len(groupValues["Maximum Execution Time"]) > 0 {
				timeUnit := groupValues["Maximum Execution Time"][len(groupValues["Maximum Execution Time"])-1]
				maxExecutionTime, err := strconv.Atoi(groupValues["Maximum Execution Time"][:len(groupValues["Maximum Execution Time"])-1])
				if err != nil {
					panic("Error in YAML file " + err.Error())
				}

				seconds := maxExecutionTime
				switch string(timeUnit) {
				case "h":
					seconds = seconds * 60 * 60
				case "m":
					seconds = seconds * 60
				}

				currentGroup.MaxExecutionTime = seconds
			}

			if len(groupValues["Sentinel"]) > 0 {
				currentGroup.SentinelCmd = groupValues["Sentinel"]
			}

			currentGroup.AverageCpuUsage = 1.0
			currentGroup.AverageMemoryUsage = 1

			*groups = append(*groups, currentGroup)
		}
	}

	return
}
