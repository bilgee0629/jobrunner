package app

import "testing"

func TestGetGroups(t *testing.T) {
	plan, err := PlanFromYaml("../tests/test.yaml")
	if err != nil {
		t.FailNow()
	}
	groups := plan.GetGroups()
	if groups == nil {
		t.Fail()
	}

}
