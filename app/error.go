package app

type AppError struct {
	Err     error
	Message string
}

func NewAppError(err error, message string) *AppError {
	return &AppError{
		err,
		message,
	}
}

func (err *AppError) String() string {
	return "Error occured: " + err.Message + " - " + err.Err.Error()
}
