package app

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
	"time"
)

var Monolithic *JobRunner

type JobRunner struct {
	RamAmount int //ram information of the host
	PageSize  int
	CpuAmount int //how many CPUs does host have
	CpuData   []*CpuData
	Groups    []*Group
	CpuTime   int
}

func NewJobRunner() *JobRunner {

	jb := JobRunner{
		RamAmount: GetRamAmount(),
		CpuAmount: GetCpuAmount(),
	}

	jb.PageSize = os.Getpagesize() / 1024

	jb.CpuData = []*CpuData{}
	for i := 0; i <= jb.CpuAmount; i++ {
		jb.CpuData = append(jb.CpuData, &CpuData{})
	}

	return &jb
}

func (jr *JobRunner) StartGroup(group *Group) (err error) {

	pid, err := StartProcess(group.Command)
	if err != nil {
		return err
	}

	group.Process = append(group.Process, &Process{Pid: pid})

	jr.Groups = append(jr.Groups, group)

	if group.MaxExecutionTime != 0 {
		go func() {
			time.Sleep(time.Duration(group.MaxExecutionTime) * time.Second)
			jr.StopGroup(group)
		}()
	}

	return nil
}

func (jr *JobRunner) StopGroup(group *Group) (err error) {

	for index, jrGroup := range jr.Groups {
		if jrGroup.Name == group.Name {
			for _, process := range jrGroup.Process {
				p, err := os.FindProcess(process.Pid)
				if err != nil {
					return err
				}
				p.Kill()
			}

			jr.Groups = append(jr.Groups[:index], jr.Groups[index+1:]...)
			break
		}
	}

	return nil
}

func (jr *JobRunner) StopAllGroup() (err error) {

	for _, group := range jr.Groups {
		err := jr.StopGroup(group)
		if err != nil {
			return err
		}
	}

	return nil
}

func (jr *JobRunner) RefreshGroups() (err *AppError) {

	Monolithic.CpuTime, err = ScanCpuTime()
	if err != nil {
		return err
	}

	for _, group := range jr.Groups {
		group.RefreshProcesses()
		group.StartSentinel()
	}

	return nil
}

func (jr *JobRunner) ManageGroups() {

	for _, group := range jr.Groups {
		go func(group *Group) {

			group.Refresh()

			if group.CurrentCpuUsage == 0 && group.CurrentMemoryUsage == 0 {
				//no process is running. Maybe one process exceeds the hardware limits
				//so we will try adding new process
				group.AddProcess()
			} else {
				//adding new processes
				freeCpu := float64(group.MaxCpuUsage) - group.CurrentCpuUsage
				freeMem := float64(group.MaxMemUsage) - group.CurrentMemoryUsage

				maxNewProcessesByCpu := int(freeCpu / group.AverageCpuUsage)
				if maxNewProcessesByCpu < 0 {
					maxNewProcessesByCpu = 0
				}

				maxNewProcessesByMem := int(freeMem / group.AverageMemoryUsage)
				if maxNewProcessesByMem < 0 {
					maxNewProcessesByMem = 0
				}

				newProcessNum := maxNewProcessesByCpu
				if maxNewProcessesByCpu > maxNewProcessesByMem {
					newProcessNum = maxNewProcessesByMem
				}

				for i := 0; i < newProcessNum; i++ {
					group.AddProcess()
				}
			}

			//removing processes if processes exceed predefined max amounts
			for group.CurrentCpuUsage > float64(group.MaxCpuUsage) || group.CurrentMemoryUsage > float64(group.MaxMemUsage) {
				group.RemoveProcess()
				group.Refresh()
			}

		}(group)
	}

}

// func (jr *JobRunner) CallSentinel() error {
// 	for
// }

func IsJobRunnerAlreadyRunning() (bool, error) {

	dir, err := os.Open("/proc")
	if err != nil {
		return false, err
	}
	defer dir.Close()

	dirs, err := dir.Readdir(0)
	if err != nil {
		return false, err
	}

	occurence := 0
	for _, currentDir := range dirs {
		if _, err := strconv.Atoi(currentDir.Name()); err == nil {
			//process dir
			stat, err := ioutil.ReadFile("/proc/" + currentDir.Name() + "/stat")
			if err != nil {
				panic(err)
			}

			split := strings.Split(string(stat), " ")
			if split[1][1:len(split[1])-1] == "jobrunner" {
				occurence++
			}
		}
	}

	if occurence > 1 {
		//jobrunner is already running
		return true, nil
	}

	return false, nil
}

func SendPlanToExistingApp(filePath string, operation string) {

	conn, err := net.Dial("tcp", "127.0.0.1:27040")
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	fmt.Fprintf(conn, operation+" "+filePath+"\n")
	for {
		message, err := bufio.NewReader(conn).ReadString('\n')
		if err != nil {
			log.Println("ERROR WHILE READING STRING OVER TCP", err)
		}
		if len(message) > 0 {
			fmt.Println("Message Receieved back", message)
			os.Exit(0)
		}
	}

}
