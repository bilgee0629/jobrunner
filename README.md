**Installation**

```sh
$ go get git.excellenceontime.ca/eric/JobRunner
$ cd $GOPATH/src/git.excellenceontime.ca/eric/JobRunner && make install
```
**To start a plan**
```sh
$ jobrunner -start="plan_file_path"
```

**To stop a plan**
```sh
$ jobrunner -stop="plan_file_path"
```

**To stop all plan**
```sh
$ jobrunner -stop=all
```
