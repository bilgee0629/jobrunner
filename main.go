package main

import (
	"bufio"
	"flag"
	"fmt"
	"net"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"git.excellenceontime.ca/eric/JobRunner/app"
)

var flagStart string
var flagStop string

var REFRESH_RATE = 5

func main() {
	app.Monolithic = app.NewJobRunner()

	fmt.Println("Crowdbotics")

	parseCmds()

	if ok, err := app.IsJobRunnerAlreadyRunning(); err == nil && ok {
		//jobrunner is already running
		fmt.Println("jobrunner is already running")
		if len(flagStart) > 0 {
			app.SendPlanToExistingApp(flagStart, "start")
		}

		if len(flagStop) > 0 {
			app.SendPlanToExistingApp(flagStop, "stop")
		}

	} else if err == nil && !ok {
		//jobrunner is not running
		fmt.Println("jobrunner is not running starting jobrunner")
		runCmds()

		app.Monolithic.RefreshGroups()

		go ListenTcpMessages()
		go func() {
			for {
				time.Sleep(time.Duration(REFRESH_RATE) * time.Second)
				app.Monolithic.RefreshGroups()
				app.Monolithic.ManageGroups()
			}
		}()

	} else if err != nil {
		panic(err)
	}

	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	<-c
}

func runCmds() {
	if len(flagStart) > 0 {
		plan, err := app.PlanFromYaml(flagStart)
		if err != nil {
			panic(err)
		}

		groups := plan.GetGroups()
		for _, group := range *groups {
			err := app.Monolithic.StartGroup(&group)
			if err != nil {
				panic(err)
			}
		}

	}
}

func parseCmds() {
	flag.Usage = func() {
		fmt.Fprintln(os.Stderr, usage)
	}

	flag.StringVar(&flagStart, "start", "", "")
	flag.StringVar(&flagStop, "stop", "", "")

	flag.Parse()
}

func ListenTcpMessages() {
	fmt.Println("Started listening on :27040")
	ln, err := net.Listen("tcp", ":27040")
	if err != nil {
		panic(err)
	}
	defer ln.Close()

	for {
		conn, _ := ln.Accept()
		defer conn.Close()

		message, _ := bufio.NewReader(conn).ReadSlice('\n')
		if len(message) > 0 {
			messageString := strings.TrimSpace(string(message))
			split := strings.Split(messageString, " ")

			if split[1] == "all" {
				//we will stop all groups
				app.Monolithic.StopAllGroup()

			} else {
				//received yaml file
				plan, err := app.PlanFromYaml(split[1])
				if err != nil {
					conn.Write([]byte("ERROR while reading plan file\n"))
				}

				groups := plan.GetGroups()
				for _, group := range *groups {
					if split[0] == "start" {
						app.Monolithic.StartGroup(&group)
					} else {
						//if operation is not start, we will stop group
						app.Monolithic.StopGroup(&group)
					}
				}
			}

			conn.Write([]byte("SUCCESS\n"))
		}
	}
}

var usage = `
    -start <path to yaml file>
    starts a job group with given yaml file

    -stop <path to yaml file>
    stops job group with given yaml file

    -stop all
    stops all jobs groups initiated by JobRunner
`
